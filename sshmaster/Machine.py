# -*- coding: utf-8 -*-
from Connection import Connection
from sys import stderr

class Machine:
    def __init__(self, ip, ssh_key, user='root'):
        self.ip = ip
        self.ssh_key = ssh_key
        self.user = user
        self.connection = None

    def get_ip(self):
        return self.ip

    def connect(self):
        self.connection = Connection(self.ip, self.user, self.ssh_key)

    def end_connection(self):
        if self.connection:
            self.connection.close()
        self.connection = None

    def is_alive(self):
        if self.connection:
            return self.connection._tranport_live
        return False

    def execute(self, command):
        if isinstance(command, str):
            return self.connection.execute(command)
        elif isinstance(command, list):
            results = []
            for c in command:
                results.append(self.connection.execute(c))
            return results
        else:
            stderr.write("Machine::execute - 'command' argument is not a list or a string\n")

    def put(self, localpath, remotepath = None):
        self.connection.put(localpath, remotepath)

    def get(self, remotepath, localpath = None):
        self.connection.get(remotepath, localpath)
