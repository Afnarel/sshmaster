# -*- coding: utf-8 -*-
from Cluster import Cluster
from time import sleep

def test1(cluster):
    cluster.show_state()
    print cluster.execute('ls')
    cluster.end_connections()
    cluster.show_state()
    print cluster.execute('ls')
    cluster.connect()
    cluster.show_state()
    print cluster.execute('ls')

def test2(cluster):
    while True:
        cluster.show_state()
        sleep(10)

def test3(cluster):
    print cluster.execute(['ls', 'pwd', 'date'])

if __name__ == '__main__':
    IPS = ['some.host.com']
    SSH_KEY = '~/.ssh/id_rsa'
    cluster = Cluster(IPS, SSH_KEY)
    cluster.keep_alive()

#    test1(cluster)
#    test2(cluster)
    test3(cluster)

