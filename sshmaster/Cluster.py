# -*- coding: utf-8 -*-
from Machine import Machine

class Cluster:
    def __init__(self, ips, ssh_key, user='root'):
        self.ips = ips
        self.ssh_key = ssh_key
        self.user = user
        self.machines = []
        for ip in self.ips:
            machine = Machine(ip, ssh_key, self.user)
            self.add_machine(machine)

    def keep_alive(self):
        """
        Launches a thread that will keep all connections alive by
        a. sending a harmless commande (date) through the connection
            at short intervals of time so that the connection doesn't
            time out
        b. checking regularly that each machine is alive and calling its
            connect() method if it's not
        """
        # TODO (seems useless for the moment)
        pass

    def add_machine(self, machine):
        """
        Adds a Machine instance to the Cluster
        Establishes a connection to the machine if this isn't done yet
        """
        if not machine.is_alive():
            machine.connect()
        self.machines.append(machine)

    def get_machines(self):
        """
        Returns the list of all the Machines tracked by this cluster
        """
        return self.machines

    def get_alive_machines(self):
        """
        Returns the list of all the Machines in this Cluster for which
        the connection is still opened
        """
        return [m for m in self.machines if m.is_alive()]

    def get_dead_machines(self):
        """
        Returns the list of all the Machines in this Cluster for which
        the connection is closed
        """
        return [m for m in self.machines if not m.is_alive()]

    def end_connections(self):
        for machine in self.get_alive_machines():
            machine.end_connection()

    def connect(self):
        for machine in self.get_dead_machines():
            machine.connect()

    def execute(self, command):
        results = {}
        for machine in self.get_alive_machines():
            results[machine.get_ip()] = machine.execute(command)
        return results

    def put(self, localpath, remotepath = None):
        for machine in self.get_alive_machines():
            machine.put(localpath, remotepath)

    def get(self, remotepath, localpath = None):
        for machine in self.get_alive_machines():
            machine.get(remotepath, localpath)

    def show_state(self):
        """
        For each Machine in the Cluster, show the state of the connection
        (alive / dead)
        """
        if not self.machines:
            print "This cluster doesn't track any machine"
        for machine in self.machines:
            print machine.get_ip() + ':',
            if machine.is_alive():
                print 'ALIVE'
            else:
                print 'DEAD'
